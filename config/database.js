//! Old local DB configuration
// module.exports = ({ env }) => ({
//   defaultConnection: 'default',
//   connections: {
//     default: {
//       connector: 'bookshelf',
//       settings: {
//         client: 'sqlite',
//         filename: env('DATABASE_FILENAME', '.tmp/data.db'),
//       },
//       options: {
//         useNullAsDefault: true,
//       },
//     },
//   },
// });

//! Postgres DB URL: postgres://sqptzkmayubjnc:ee4c0cf724bee78ff02b38b66800989ed5fbf7c753325c1078cc57ea09f9e562@ec2-54-155-92-75.eu-west-1.compute.amazonaws.com:5432/derdpd0eunt387
module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'postgres',
        host: env('DATABASE_HOST', 'ec2-54-155-92-75.eu-west-1.compute.amazonaws.com'),
        port: env.int('DATABASE_PORT', 5432),
        database: env('DATABASE_NAME', 'derdpd0eunt387'),
        username: env('DATABASE_USERNAME', 'sqptzkmayubjnc'),
        password: env('DATABASE_PASSWORD', 'ee4c0cf724bee78ff02b38b66800989ed5fbf7c753325c1078cc57ea09f9e562'),
        ssl: {
          rejectUnauthorized: false,
        },
      },
      options: {
        ssl: true,
      },
    },
  },
});